package com.epam;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class InputText {
    public void scanText(){
        Scanner keyboard = new Scanner(System.in);
        String lineWord = keyboard.nextLine();
        Pattern p;
        p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(lineWord);

        while ( m.find() ) System.out.println(lineWord.substring(m.start(), m.end()));
    }
}